### Android Resources

This is a simple curated list of things relating to android development that might be of interest to people who are getting started, or looking to dive deeper.

#### Dependency Injection
[Dagger](http://square.github.io/dagger/)

#### Kotlin

[Article on FP in Kotlin](https://blog.plan99.net/kotlin-fp-3bf63a17d64a)